<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>



<div class="container-sm">
    <div class="container mt-5">
        <form action="{{route('uploadFile')}}" method="post" enctype="multipart/form-data">
            @csrf
            <label for="">Upload Your CSV file here : </label>
            <input type="file" class="form-control" name="file">
            <button class="btn btn-sm btn-dark" type="submit ">Submit</button>
        </form>
    </div>
    <div class="container mt-5">
        <table class="table table-hover">

            <tr>
                <th>Time</th>
                <th>File Name</th>
                <th>Status</th>
            </tr>
            @forelse ($file as $f )
            <tr>
                <td>{{'time'}}</td>
                <td>{{$f->name}}</td>
                <td>{{'status'}}</td>
            </tr>
                
            @empty
                
            @endforelse

        </table>
      </div>
</div>