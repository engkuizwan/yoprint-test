<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \PhpOffice\PhpSpreadsheet\Spreadsheet;
use \PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use \PhpOffice\PhpSpreadsheet\IOFactory;
class MainController extends Controller
{
    //
    public function uploadFile(Request $request)
    {
 
        $file = $request->file('file');
// dd($file->getClientOriginalName());
        // Read the CSV file and convert it to an array.
        $csvData = file($file->getRealPath());
        $csvData = array_map('str_getcsv', $csvData);
        unset($csvData[0]);
        // dd($csvData[1][2]);

        try{
            DB::table('file')->insert([
                'name' => $file->getClientOriginalName(),
                'path' => 'test'
            ]);

            foreach ($csvData as $data) {
                $dataExisting = DB::table('product')->where('UNIQUE_KEY',intval($data[0]))->first();
                // dd($dataExisting);
                if($dataExisting){
                    DB::table('product')->where('UNIQUE_KEY',intval($data[0]))->update([
        
                        'UNIQUE_KEY' => intval($data[0]),
                        'PRODUCT_TITLE' =>  iconv("UTF-8","UTF-8//IGNORE",$data[1]),
                        'PRODUCT_DESCRIPTION' =>  iconv("UTF-8","UTF-8//IGNORE",$data[2]),
                        'STYLE#' =>  iconv("UTF-8","UTF-8//IGNORE",$data[3]),
                        'SANMAR_MAINFRAME_COLOR' =>  iconv("UTF-8","UTF-8//IGNORE",$data[28]),
                        'SIZE' =>  iconv("UTF-8","UTF-8//IGNORE",$data[18]),
                        'COLOR_NAME' =>  iconv("UTF-8","UTF-8//IGNORE",$data[14]),
                        'PIECE_PRICE' =>  iconv("UTF-8","UTF-8//IGNORE",$data[21]),
        
                    ]);
                }else{
                    DB::table('product')->insert([
        
                        'UNIQUE_KEY' => intval($data[0]),
                        'PRODUCT_TITLE' => $data[1],
                        'PRODUCT_DESCRIPTION' => $data[2],
                        'STYLE#' => $data[3],
                        'SANMAR_MAINFRAME_COLOR' => $data[28],
                        'SIZE' => $data[18],
                        'COLOR_NAME' => $data[14],
                        'PIECE_PRICE' => $data[21],
        
                    ]);
                }
            }
    
            return redirect(route('home'))->withSuccess('Saved!');

        }catch(\Throwable $th){
            dd($th);
            return back()->withError('Something when wrong!');
        }
    }
}



